import numpy as np
from scipy.interpolate import UnivariateSpline
from scipy.signal import savgol_filter

import peakutils

from model.algorithm import Algorithm, Parameter

class Approximator(Algorithm):

    def __init__(self, interval_length=100):
        super(Approximator, self).__init__()
        self.parameters['Interval length'] = Parameter(
            value=interval_length, range=[i for i in range(100, 2000, 1)], default_value=interval_length
        )

    def find_baseline(self, signal):
        pass

class UnivariateSplineApproximator(Approximator):

    def __init__(self, interval_length=100):
        super(UnivariateSplineApproximator, self).__init__(interval_length)
        self.name = 'UnivariateSpline'

    def find_baseline(self, signal):
        baseline = np.zeros((len(signal),))
        for x_0 in range(0, len(signal), self.parameters['Interval length'].value):
            x_1 = np.clip(x_0 + self.parameters['Interval length'].value, x_0 + 1, len(signal))
            current_dots = signal[x_0:x_1]
            s = UnivariateSpline(np.asarray([x for x in range(len(current_dots))]), current_dots, s=1)
            baseline[x_0:x_0 + len(current_dots)] = s(np.asarray([x for x in range(len(current_dots))]))
        return baseline

class Fourier(Approximator):

    def __init__(self, interval_length=100, number_of_coeffs=10):
        super(Fourier, self).__init__(interval_length)
        self.name = 'Fourier'
        self.parameters['Number of coefficients'] = Parameter(
            value=number_of_coeffs, range=[i for i in range(1, 200, 1)], default_value=number_of_coeffs
        )

    def find_baseline(self, signal):
        if len(signal.shape) > 1:
            dots = signal[:, 0]
        else:
            dots = signal
        baseline = np.zeros((dots.shape[0],))

        for x_0 in range(0, len(dots), self.parameters['Interval length'].value):

            current_dots = dots[x_0:x_0 + self.parameters['Interval length'].value]

            self.fourier_series = {'a': [0] * (self.parameters['Number of coefficients'].value + 1),
                                   'b': [0] * self.parameters['Number of coefficients'].value}

            L = len(current_dots) // 2

            self.fourier_series['a'][0] = (1 / L) * np.sum(current_dots)

            for n in range(1, self.parameters['Number of coefficients'].value):
                self.fourier_series['a'][n] = \
                    (1 / L) * sum([current_dots[x] * np.cos(x * n * np.pi / L)
                                   for x in range(-L, L)])
                self.fourier_series['b'][n - 1] = \
                    (1 / L) * sum([current_dots[x] * np.sin(x * n * np.pi / L)
                                   for x in range(-L, L)])
            baseline[x_0:x_0 + self.parameters['Interval length'].value] = np.asarray(
                [
                    self.fourier_series['a'][0] / 2 +
                    sum([
                        self.fourier_series['a'][n] * np.cos(np.pi * n * x / L) +
                        self.fourier_series['b'][n - 1] * np.sin(np.pi * n * x / L)
                        for n in range(1, self.parameters['Number of coefficients'].value)])
                    for x in range(current_dots.shape[0])
                ],
                np.float64)
        return baseline

class Polynomial(Approximator):

    def __init__(self, interval_length=100, degree=5, max_it=1000):
        super(Polynomial, self).__init__(interval_length)
        self.name = 'Polynomial'
        self.parameters['Degree'] = Parameter(
            value=degree, range=[i for i in range(0, 10, 1)], default_value=degree
        )
        self.parameters['Number of iterations'] = Parameter(
            value=max_it, range=[i for i in range(10000)], default_value=max_it
        )

    def find_baseline(self, signal):
        if len(signal.shape) > 1:
            dots = signal[:, 0]
        else:
            dots = signal
        baseline = np.zeros((dots.shape[0],))

        for x_0 in range(0, len(dots), self.parameters['Interval length'].value):

            baseline[x_0:x_0 + self.parameters['Interval length'].value] = peakutils.baseline(
                y=dots[x_0:x_0 + self.parameters['Interval length'].value],
                deg=self.parameters['Degree'].value,
                max_it=self.parameters['Number of iterations'].value
            )
        return baseline

class SavitzkyGolay(Approximator):

    def __init__(self, interval_length=100, degree=3):
        super(SavitzkyGolay, self).__init__(interval_length=interval_length)
        self.name = 'SavitzkyGolay'
        self.parameters['Degree'] = Parameter(
            value=degree, range=[i for i in range(0, 10, 1)], default_value=degree
        )

    def find_baseline(self, signal):
        window_size = self.parameters['Interval length'].value
        if window_size % 2 == 0:
            window_size += 1
        baseline = savgol_filter(signal, window_size, self.parameters['Degree'].value)
        return baseline
