from view.gui import UserGui, DataAnalysisDialog, DataAnalysisPlot
from model.data_model import SignalModel
from controller.gui_out import FromUserGui

if __name__ == '__main__':
    model = SignalModel()
    main_view = UserGui()
    data_analysis_dialogs = {}
    data_analysis_plots = {}
    options = ['peak_analysis', 'cluster_analysis', 'classification', 'peak_visualization']
    for option in options:
        data_analysis_dialogs[option + '_dialog'] = DataAnalysisDialog()
        data_analysis_plots[option + '_plot'] = DataAnalysisPlot()
    controller = FromUserGui(model, main_view, data_analysis_dialogs, data_analysis_plots)
    controller.run()