import os
import numpy as np
from io import StringIO
from numpy import genfromtxt

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def read_from_lvm(filenames, delimiter='\t', symbols_to_delete=[]):
    data = {}
    for filename in filenames:
        f = open(filename)
        lines = f.readlines()
        header_candidate = lines[0].split(delimiter)
        header_is_found = False
        initial_index = 0
        final_index = len(lines)
        for col in header_candidate:
            if not is_number(col):
                header_is_found = True
        if header_is_found:
            header = header_candidate
            for num, col in enumerate(header):
                header[num] = col.strip('\n')
                for symbol in symbols_to_delete:
                    header[num] = header[num].replace(symbol, '')
            initial_index = 2
            final_index -= 1
            text = '\n'.join(lines[initial_index:final_index])
            text = text.replace('\t', ' ')
            text = text.replace(',', '.')
            data[filename] = np.loadtxt(StringIO(text))
        else:
            max_line_length = 0
            for line in lines:
                cols = line.split('\t')
                if max_line_length < len(cols):
                    max_line_length = len(cols)
            for num, line in enumerate(lines):
                difference = max_line_length - len(line.split('\t'))
                if difference > 0:
                    nan_values = ''
                    for _ in range(difference):
                        nan_values += ' , '
                    lines[num] = '{}{}\n'.format(line.strip('\n').replace('\t', ','), nan_values)
                else:
                    lines[num] = line.replace('\t', ',')
            directory, file = os.path.split(filename)
            temp = open(os.path.join(directory, 'temp.lvm'), 'w+')
            temp.writelines(lines)
            data[filename] = genfromtxt(os.path.join(directory, 'temp.lvm'), delimiter=',')
            temp.close()
            os.remove(os.path.join(directory, 'temp.lvm'))
            header = ['column_{}'.format(i) for i in range(max_line_length)]
    same_headers = {}
    for num, col in enumerate(header):
        if col not in same_headers:
            same_headers[col] = 1
        else:
            header[num] = '{}_{}'.format(col, same_headers[col])
            same_headers[col] += 1
    return header, data
