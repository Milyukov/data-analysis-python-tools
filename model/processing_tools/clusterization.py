from model.processing_tools.data_analysis import *
from model.algorithm import Algorithm, Parameter

from sklearn.cluster import KMeans

class Clusterizator(Algorithm):

    def __init__(self):
        super(Clusterizator, self).__init__()

    def clusterize(self, data, show_absolute_values=False):
        pass

class KMeansAnalysis(Clusterizator):

    def __init__(self, k=3):
        super(KMeansAnalysis, self).__init__()
        self.parameters['K'] = Parameter(
            value=k,
            range=[i for i in range(1, 31)],
            default_value=k
            #TODO: add type
        )

    def clusterize(self, data, show_absolute_values=False):
        # pick paired peaks (3P)
        indecies = np.logical_not(np.logical_or(np.isnan(data[:, 1]), np.isnan(data[:, 0])))
        cluster_mask = np.zeros_like(data[:, 0])
        data = data[indecies, :]
        if show_absolute_values:
            data = np.abs(data)

        k_means_engine = KMeans(n_clusters=int(self.parameters['K'].value), n_jobs=10)
        k_means_engine.fit(data)
        clusters = k_means_engine.predict(data)

        cluster_mask[indecies] = clusters
        return cluster_mask.astype(np.int)

class DensityAnalysis(Clusterizator):

    def __init__(self, kernel_size=40, threshold_value=40):
        super(DensityAnalysis, self).__init__()
        self.parameters['Kernel size'] = Parameter(
            value=kernel_size,
            range=[i for i in range(3, 100, 1)],
            default_value=kernel_size
        )
        self.parameters['Threshold value'] = Parameter(
            value=threshold_value,
            range=[i for i in range(10, 101, 1)],
            default_value=threshold_value
        )

    def clusterize(self, data, show_absolute_values=False):

        # pick paired peaks (3P)
        # TODO: mark non-paired peaks somehow different
        indecies = np.logical_not(np.logical_or(np.isnan(data[:, 1]), np.isnan(data[:, 0])))
        cluster_mask = np.zeros_like(data[:, 0])
        data = data[indecies, :]
        if show_absolute_values:
            data = np.abs(data)

        threshold_value = self.parameters['Threshold value'].value
        kernel_size = self.parameters['Kernel size'].value

        # perform density analysis
        density_map, yedges, xedges = np.histogram2d(data[:, 0], data[:, 1], bins=data.shape[0] / kernel_size)

        # separate high and low density clusters
        ys, xs = np.where(density_map >= threshold_value)

        mask = np.zeros_like(data[:, 0])
        for y, x in zip(ys, xs):
            y_mask = np.logical_and(data[:, 0] > yedges[y], data[:, 0] < yedges[y + 1])
            x_mask = np.logical_and(data[:, 1] > xedges[x], data[:, 1] < xedges[x + 1])
            mask += y_mask * x_mask
        mask += 1
        cluster_mask[indecies] = mask
        return cluster_mask.astype(np.int)
