from model.processing_tools.data_analysis import *
from model.algorithm import Algorithm, Parameter

import subprocess
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn.ensemble import iforest
import pickle

from scipy.ndimage.morphology import binary_dilation, binary_opening

class Classifier(Algorithm):

    def __init__(self):
        super(Classifier, self).__init__()
        self.model = None
        self.outlier_detector = None
        self.mean_val = 0.0
        self.std_val = 1.0
        self.label_names = []

    def train(self, features, labels, label_names=None):
        pass

    def save(self, filename, visualize=False):
        # save the model to disk
        pickle.dump(
            {
                'model': self.model,
                'outlier_detector': self.outlier_detector,
                'mean_val': self.mean_val,
                'std_val': self.std_val,
                'label_names': self.label_names
            },
            open(filename, 'wb')
        )

    def load(self, filename):
        loaded_data = pickle.load(open(filename, 'rb'))
        self.model = loaded_data['model']
        self.outlier_detector = loaded_data['outlier_detector']
        self.mean_val = loaded_data['mean_val']
        self.std_val = loaded_data['std_val']
        self.label_names = loaded_data['label_names']

    def classify(self, data):
        pass

class DecisionTree(Classifier):

    def __init__(self):
        super(DecisionTree, self).__init__()

    def train(self, features, labels, label_names):
        """
        Works with raw (not normalized) data
        :param features: data (peak values)
        :param labels: class numbers for each peak pair
        :return:
        """
        if label_names is not None:
            self.label_names = label_names
            self.label_names.update({0: 'outliers'})

        indecies = np.logical_not(
            np.logical_or(np.isnan(features[:, 1]), np.isnan(features[:, 0]))
        )
        # init mean and std values
        normalized_features, self.mean_val, self.std_val = normalize_data(features[indecies])
        # init models
        self.outlier_detector = iforest.IsolationForest(max_samples=1.0)
        self.model = DecisionTreeClassifier()
        # learn data distribution
        self.outlier_detector.fit(normalized_features)
        train_data = normalized_features
        train_labels = labels[indecies]

        # add labeled outliers
        # get density heat map of train dots
        magnifier = 1.5
        density, accuracy, miny, minx, maxy, maxx = get_density_from_points_(
            normalized_features, magnifier=magnifier
        )

        # dilate it
        dilated_density_map = binary_dilation(density > 0, np.ones((10, 10)))

        # get difference between dilated and original heat map
        difference = ~(dilated_density_map > 0)  # ~(density > 0) |
        difference = binary_opening(difference, np.ones((3, 3)))

        # convert it to points
        outliers_data = get_points_from_density_(difference, maxy, maxx, miny, minx, accuracy, magnifier=magnifier)
        outliers_labels = np.zeros((outliers_data.shape[0],))
        outliers_labels = outliers_labels.astype(np.int)
        # add them to train data as outliers (-1)
        train_data = np.concatenate((train_data, outliers_data))
        train_labels = np.concatenate((train_labels, outliers_labels))
        # add noise to data and concatenate the result with original set
        train_data_noised = (
                train_data +
                np.random.normal(0, 0.1, train_data.shape)
        )

        train_data = np.concatenate(
            (train_data, train_data_noised)
        )
        train_labels = np.concatenate(
            (train_labels, train_labels)
        )
        # train Decision Tree
        self.model.fit(train_data, train_labels)

    def save(self, filename, visualize=False):
        super(DecisionTree, self).save(filename, visualize)
        if visualize:
            # converting into the pdf file
            with open("{}_classifier.dot".format(filename), "w") as f:
                export_graphviz(self.model, out_file=f)
            print(subprocess.check_output([
                'dot',
                '-Tpdf',
                '{}_classifier.dot'.format(filename),
                '-o',
                '{}_classifier.pdf'.format(filename)]))

            with open("{}_isolation_forest.dot".format(filename), "w") as f:
                export_graphviz(self.outlier_detector, out_file=f)

    def classify(self, data):
        # pick paired peaks (3P)
        indecies = np.logical_not(np.logical_or(np.isnan(data[:, 1]), np.isnan(data[:, 0])))
        cluster_mask = np.zeros_like(data[:, 0])
        normalized_data, _, _ = normalize_data(data[indecies, :])
        #normalized_data = normalize_data_(data[indecies, :], self.mean_val, self.std_val)
        # find outliers
        #without_outliers = self.outlier_detector.predict(normalized_data)
        # classify
        labels = self.model.predict(normalized_data)
        #labels[without_outliers == 0] = -1

        cluster_mask[indecies] = labels
        return cluster_mask.astype(np.int)
