import numpy as np
from scipy.signal import convolve2d

def normalize_data(data):
    normalized_data = data.copy()
    # normalize data
    normalized_data, mean_val, std_val = normalize_data_(normalized_data)
    return normalized_data, mean_val, std_val

def normalize_data_(data, mean_val=None, std_val=None):
    if (mean_val is not None) and (std_val is not None):
        normalized_data = data - mean_val
        normalized_data /= std_val
        return normalized_data
    else:
        mean_val = np.mean(data, axis=0, keepdims=True)
        normalized_data = data - mean_val
        std_val = np.std(normalized_data, axis=0, keepdims=True)
        normalized_data /= std_val
        return normalized_data, mean_val, std_val

def denormalize_data_(normalized_data, std_value, mean_value):
    data = std_value * normalized_data + mean_value
    return data

def process_data(dataframe, header_exists=True):
    """

    :param dataframe:
    :return:
    """
    if header_exists:
        dataframe.replace('--', np.nan)
        for column in dataframe:
            dataframe[column] = dataframe[column].apply(lambda x: np.nan if x == '--' else x)
            dataframe[column] = dataframe[column].apply(lambda x: np.nan if x == '' else x)
    #dataframe = dataframe.dropna()
    return dataframe

def get_min_max_values_(data):
    '''

    :param data:
    :return:  miny, minx, maxy, maxx
    '''
    miny, maxy = np.min(data[:, 0]), np.max(data[:, 0])
    minx, maxx = np.min(data[:, 1]), np.max(data[:, 1])
    return miny, minx, maxy, maxx

def get_density_from_points_(data, magnifier=1.1):
    miny, minx, maxy, maxx = get_min_max_values_(data)
    kernel_size = 10
    height = maxy - miny
    width = maxx - minx
    accuracy = (100.0 / height, 100.0 / width)
    height = np.int(height * accuracy[0]) + 1
    width = np.int(width * accuracy[1]) + 1
    # generate spectrum
    spectrum = np.zeros((int(height * magnifier), int(width * magnifier)))
    for y, x in zip(data[:, 0] - miny, data[:, 1] - minx):
        spectrum[
            np.int(y * accuracy[0] + height * (magnifier - 1.0) / 2),
            np.int(x * accuracy[1] + width * (magnifier - 1.0) / 2)
        ] = 1.
    # evaluate points density
    density = convolve2d(spectrum, np.ones((kernel_size, kernel_size), dtype=np.float64), mode='same')
    return density, accuracy, miny, minx, maxy, maxx

def get_points_from_density_(spectrum, maxy, maxx, miny, minx, accuracy, magnifier=1.1):
    y, x = np.where(spectrum > 0)
    data = np.zeros((y.shape[0], 2))
    data[:, 0] = miny + (y - (maxy - miny) * accuracy[0] * (magnifier - 1.0) / 2) / accuracy[0]
    data[:, 1] = minx + (x - (maxx - minx) * accuracy[1] * (magnifier - 1.0) / 2) / accuracy[1]
    return data

if __name__ == '__main__':
    pass