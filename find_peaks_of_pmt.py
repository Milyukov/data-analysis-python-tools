import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from model.data_model import SignalModel
from model.processing_tools.lvm_reader import read_from_lvm

def visualize_peaks(column):
    figure = plt.figure()
    ax1 = figure.add_subplot(111)
    ax2 = ax1.twinx()
    colors = ['tab:green', 'tab:blue']
    peaks_indices = np.logical_not(np.isnan(column['peaks']))
    x = [i for i in range(len(column['signal']))]
    ax1.plot(x, column['signal'] - np.mean(column['signal']), label='{}'.format(column['column_name']),
                  color=colors[0])
    ax1.scatter(x=np.nonzero(peaks_indices)[0], y=column['signal'][peaks_indices] - np.mean(column['signal']), c='r')
    ax1.set_xlabel('Sample number')
    ax1.set_ylabel('{}'.format(column['column_name']))

    ax1.ticklabel_format(style='sci', axis='x', scilimits=(-3, 9))
    ax1.ticklabel_format(style='sci', axis='y', scilimits=(-3, 9))
    ax1.legend(loc='best')
    plt.show()

def visualize_baseline(col):
    figure = plt.figure()
    number_of_rows = 1
    number_of_cols = 1
    id = 0
    coupled_peaks_indices = np.logical_not(np.isnan(col['peaks']))
    if col['baseline'] is not None:
        xs = [i for i in range(len(col['signal']))]
        ax = figure.add_subplot(
            number_of_rows * 2, number_of_cols, 2 * id + 1
        )
        ax.plot(xs, col['signal'])
        ax.plot(xs, col['baseline'])
        x = coupled_peaks_indices
        ax.scatter(x=np.nonzero(x)[0], y=col['signal'][x], c='r')
        ax.set_ylabel(col['column_name'])
        ax.set_xlabel('Sample number')
        ax.ticklabel_format(style='sci', axis='x', scilimits=(-3, 9))
        ax.ticklabel_format(style='sci', axis='y', scilimits=(-3, 9))
        ax = figure.add_subplot(
            number_of_rows * 2, number_of_cols, 2 * id + 1 + number_of_cols
        )
        ax.plot(xs, col['signal'] - col['baseline'])
        ax.scatter(x=np.nonzero(x)[0], y=col['signal'][x] - col['baseline'][x], c='r')
    else:
        xs = [i for i in range(len(col['signal']))]
        ax = figure.add_subplot(
            number_of_rows, number_of_cols, id + 1
        )
        ax.plot(xs, col['signal'])
        x = coupled_peaks_indices
        ax.scatter(x=np.nonzero(x)[0], y=col['signal'][x], c='r')
    for iqr_label in col['iqr_area']:
        iqr_y = col['iqr_area'][iqr_label]
        ax.plot([0, len(xs)], [iqr_y, iqr_y], '--o', label=iqr_label)
    ax.set_ylabel('{} with subtracted baseline'.format(col['column_name']))
    ax.set_xlabel('Sample number')
    ax.ticklabel_format(style='sci', axis='x', scilimits=(-3, 9))
    ax.ticklabel_format(style='sci', axis='y', scilimits=(-3, 9))
    ax.legend()
    plt.show()

if __name__ == '__main__':
    # read command line arguments
    parser = argparse.ArgumentParser(description='Process input data paths and algorithm parameters')
    parser.add_argument('spec_file_path', metavar='spec_path', type=str, help='Path to a spec file')
    parser.add_argument('pmt_file_path', metavar='pmt_path', type=str, help='Path to a PMT file')
    parser.add_argument('output_file_path', metavar='output_file', type=str, help='Path to output excel file')
    args = parser.parse_args()

    # read spec file
    header, data = read_from_lvm([args.spec_file_path])
    concatenated_data = None
    for key in data:
        if concatenated_data is None:
            concatenated_data = data[key]
        else:
            concatenated_data = np.concatenate((concatenated_data, data[key]))

    spec_model = SignalModel()
    spec_model.set_data_frame(pd.DataFrame(columns=header, data=concatenated_data))

    # TODO: remove outliers (lonely peaks by mistake)

    # for each droplet
    spec_data = {}
    time = spec_model.data_frame['Time']
    for col in spec_model.data_frame:
        print('Processing {} column..'.format(col))
        if col == 'Time':
            continue
        spec_data[col] = {}
        spec_model.data_frame[col] = spec_model.data_frame[col].values
        droplet = spec_model.data_frame[col].values
        spec_data[col]['column_name'] = col
        spec_data[col]['signal'] = droplet

        # find baseline
        spec_model.find_baseline(col, algorithm_name='SavitzkyGolay')
        spec_data[col]['baseline'] = spec_model.data_frame['{}_baseline'.format(col)]

        # subtract baseline
        # find peaks
        spec_model.peak_finder['IQR'].parameters['Positive inner IQR coefficient'].value = 0.2
        spec_model.peak_finder['IQR'].parameters['Positive outer IQR coefficient'].value = 3
        spec_model.peak_finder['IQR'].parameters['Negative inner IQR coefficient'].value = 0
        spec_model.peak_finder['IQR'].parameters['Negative outer IQR coefficient'].value = 0
        spec_model.find_peaks(col, algorithm_name='IQR', ignore_zeros=True)
        peak_values = spec_model.data_frame['{}_peaks'.format(col)]
        first_peak_found = True
        max_val_index = None
        for num, peak_value in enumerate(peak_values):
            if not np.isnan(peak_value):
                if first_peak_found:
                    max_val = peak_value
                    max_val_index = num
                    first_peak_found = False
                else:
                    if max_val < peak_value:
                        max_val = peak_value
                        max_val_index = num
                peak_values[num] = np.nan
            else:
                if max_val_index is not None:
                    peak_values[max_val_index] = max_val
                    max_val_index = None
                    first_peak_found = True
        spec_data[col]['peaks'] = peak_values
        spec_data[col]['iqr_area'] = spec_model.peak_finder['IQR'].iqr_area
        #visualize_peaks(spec_data[col])
        #visualize_baseline(spec_data[col])

    # get peaks values from PMT
    header, data = read_from_lvm([args.pmt_file_path])
    concatenated_data = None
    for key in data:
        if concatenated_data is None:
            concatenated_data = data[key]
        else:
            concatenated_data = np.concatenate((concatenated_data, data[key]))

    pmt_model = SignalModel()
    pmt_model.set_data_frame(pd.DataFrame(columns=header, data=concatenated_data))

    # find corresponding peaks
    pmt_data = np.zeros((2, 0))
    time_stamps = pmt_model.data_frame['Time']
    loop_time = np.max(time_stamps)
    for num, col in enumerate(pmt_model.data_frame):
        print('Processing {} column..'.format(col))
        if col == 'Time':
            continue
        indicies = np.isnan(spec_data[col]['peaks'])
        time_values = np.expand_dims(time_stamps[indicies] + num * loop_time, axis=0)
        pmt_values = np.expand_dims(pmt_model.data_frame[col].values[indicies], axis=0)
        pmt_part = np.concatenate((time_values, pmt_values), axis=0)
        pmt_data = np.concatenate((pmt_data, pmt_part), axis=1)

    #save data
    for index in range(0, pmt_data.shape[1], 50000):
        data_frame = pd.DataFrame(pmt_data[:, index:index + 10000])
        writer = pd.ExcelWriter('{}_{}.xlsx'.format(args.output_file_path, index))
        data_frame.to_excel(writer, 'PMT peaks')
        writer.save()
