from model.algorithm import Algorithm, Parameter
import numpy as np
import peakutils

class PeakDetector(Algorithm):

    def __init__(self):
        super(PeakDetector, self).__init__()

    def detect(self, dots, ignore_zeros=False):
        pass

class DerivativeDetector(PeakDetector):

    def __init__(self, threshold=0.05, min_dist=1):
        super(DerivativeDetector, self).__init__()
        self.name = 'Derivative'
        self.parameters['Threshold'] = Parameter(
            value=threshold, range=[i / 100. for i in range(0, 101, 1)], default_value=threshold
        )
        self.parameters['Minimal distance between peaks'] = Parameter(
            value=min_dist, range=[i for i in range(0, 100, 1)], default_value=min_dist
        )

    def detect(self, dots, ignore_zeros=False):
        return peakutils.indexes(dots, thres=self.parameters['Threshold'].value, min_dist=self.parameters['Minimal distance between peaks'].value)

class OutliersDetector(PeakDetector):

    def __init__(self, positive_inner_coeff=0.5, positive_outer_coeff=1.5, negative_inner_coeff=0.5, negative_outer_coeff=1.5):
        super(OutliersDetector, self).__init__()
        self.name = 'IQR'
        self.parameters['Positive inner IQR coefficient'] = Parameter(
            value=positive_inner_coeff, range=[i / 10.0 - 5.0 for i in range(101)], default_value=positive_inner_coeff
        )
        self.parameters['Positive outer IQR coefficient'] = Parameter(
            value=positive_outer_coeff, range=[i / 10.0 - 5.0 for i in range(101)], default_value=positive_outer_coeff
        )
        self.parameters['Negative inner IQR coefficient'] = Parameter(
            value=negative_inner_coeff, range=[i / 10.0 - 5.0 for i in range(101)], default_value=negative_inner_coeff
        )
        self.parameters['Negative outer IQR coefficient'] = Parameter(
            value=negative_outer_coeff, range=[i / 10.0 - 5.0 for i in range(101)], default_value=negative_outer_coeff
        )
        self.iqr_area = None

    def detect(self, dots, ignore_zeros=False):
        positive_inner_coeff = self.parameters['Positive inner IQR coefficient'].value
        positive_outer_coeff = self.parameters['Positive outer IQR coefficient'].value
        negative_inner_coeff = self.parameters['Negative inner IQR coefficient'].value
        negative_outer_coeff = self.parameters['Negative outer IQR coefficient'].value
        if ignore_zeros:
            Q1 = np.percentile(dots[dots != 0], 25)
            Q3 = np.percentile(dots[dots != 0], 75)
        else:
            Q1 = np.percentile(dots, 25)
            Q3 = np.percentile(dots, 75)
        IQR = Q3 - Q1

        lower_inner_fence = Q1 + negative_inner_coeff * IQR
        upper_inner_fence = Q3 - positive_inner_coeff * IQR
        lower_outer_fence = Q1 - negative_outer_coeff * IQR
        upper_outer_fence = Q3 + positive_outer_coeff * IQR

        self.iqr_area = {
            'Lower inner fence': lower_inner_fence,
            'Upper inner fence': upper_inner_fence,
            'Lower outer fence': lower_outer_fence,
            'Upper outer fence': upper_outer_fence
        }

        mask1 = np.logical_and(dots > lower_outer_fence, dots < lower_inner_fence)
        mask2 = np.logical_and(dots > upper_inner_fence, dots < upper_outer_fence)
        peak_candidates = mask1 | mask2
        return np.nonzero(peak_candidates)
