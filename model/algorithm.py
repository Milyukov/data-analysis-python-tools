class AlgorithmParameters(object):

    def __init__(self):
        pass

class Parameter(object):

    def __init__(self, value, range, default_value):
        self.value = value
        self.range = range
        self.default_value = default_value

class Algorithm(object):

    def __init__(self, name=None):
        self.name = name
        self.parameters = {}
