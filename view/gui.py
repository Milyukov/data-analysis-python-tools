import tkinter
from tkinter import filedialog
from tkinter import *

import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure

import sys

# TODO:
# 1) main gui plot legend
# 5) Nyquist plot
# 6) Bode plot
# 9) delighter: exe file
# 12) initial values
# 13) measurement units
# 14) expanding list
# 15) resize canvas with window
# 16) add axis names choice to all plots

# a subclass of Canvas for dealing with resizing of windows
# TODO:
def resize_callback(event):
    new_height, new_width = event.height, event.width
    # remember old size
    # change figure size accordingly to new values
    print('Canvas shape is {}x{}'.format(new_height, new_width))

class GuiElement(object):

    def __init__(self):
        pass

    def open_file_dialog(self):
        return filedialog.askopenfilenames(filetypes=(("Excel tables", "*.xlsx"),
                                                     ("Dat files", ["*.dat", "*.DAT"]),
                                                     ("Text files", "*.txt"),
                                                     ("All files", "*.*")))

    def save_file_dialog(self):
        self.saved_filename = None
        f = filedialog.asksaveasfile(
            mode='w',
            defaultextension=".xlsx",
            filetypes=(("Excel tables", "*.xlsx"), ("All files", "*.*"))
        )
        if f is None:  # asksaveasfile return `None` if dialog closed with "cancel".
            return
        self.saved_filename = f.name
        f.close

    def open_classifier_dialog(self):
        return filedialog.askopenfilename(filetypes=(("Pickle files", "*.pickle")
                                              , ("All files", "*.*")))

    def save_classifier_dialog(self):
        self.saved_filename = None
        f = filedialog.asksaveasfile(
            mode='w',
            defaultextension=".pickle",
            filetypes=(("Pickle files", "*.pickle"), ("All files", "*.*"))
        )
        if f is None:  # asksaveasfile return `None` if dialog closed with "cancel".
            return
        self.saved_classifier_filename = f.name
        f.close

class DataAnalysisPlot(GuiElement):

    def __init__(self):
        super(DataAnalysisPlot, self).__init__()
        self.figure = Figure(figsize=(10, 7), dpi=100)

    def run(self, algorithms, callbacks, columns):
        # TODO: add axis choice
        self.top = tkinter.Tk(className='Results plotting')
        self.top.protocol("WM_DELETE_WINDOW", lambda: callbacks['on_exit_callback'](self.top))
        # menu button
        self.menu_button = Menubutton(self.top, text="File", relief=RAISED)
        self.menu_button.pack(side=tkinter.LEFT, anchor='nw', padx=3, pady=3)
        self.menu_button.menu = Menu(self.menu_button, tearoff=0)
        self.menu_button["menu"] = self.menu_button.menu
        self.menu_button.menu.add_command(label="Save",
                                          command=callbacks['save_data_callback'])
        self.menu_button.menu.add_command(label="Exit",
                                          command=lambda: callbacks['on_exit_callback'](self.top))
        # canvas to draw a plot onto
        self.canvas = FigureCanvasTkAgg(self.figure, self.top, resize_callback=resize_callback)
        NavigationToolbar2Tk(self.canvas, self.top)
        self.canvas.get_tk_widget().pack(side=tkinter.RIGHT, anchor='center', padx=3, pady=3, expand=True)
        self.canvas.draw()

        self.recalc_button = Button(self.top, text='Recalculate', command=callbacks['recalculate_callback'])
        self.recalc_button.pack(anchor='nw', padx=3, pady=3)
        self.abs_button = Button(self.top, text='abs', command=callbacks['abs_callback'], state='normal')
        self.abs_button.pack(side=tkinter.RIGHT, anchor='nw', padx=3, pady=3)

        self.algorithm_spinboxes = []
        self.algorithm_labels = []
        for algorithm in algorithms:
            Label(self.top, text=algorithm.name).pack(anchor='w')
            spinboxes = []
            labels = []
            for parameter in list(algorithm.parameters.keys()):
                labels.append(Label(self.top, text=parameter))
                labels[-1].pack(anchor='w')
                # TODO: continue developing
                text_var = IntVar(self.top, value=algorithm.parameters[parameter].default_value)
                spinboxes.append(
                    Spinbox(
                        self.top,
                        values=algorithm.parameters[parameter].range,
                        textvariable=text_var
                    )
                )
                spinboxes[-1].setvar(name='default_value', value=str(algorithm.parameters[parameter].default_value))
                spinboxes[-1].pack(anchor='w')
            self.algorithm_spinboxes.append(spinboxes)
            self.algorithm_labels.append(labels)
        Label(self.top, text='Data frame now contains the following columns:').pack(anchor='nw')
        for column in columns:
            Label(self.top, text=column, anchor="w").pack(anchor='ne')

class DataAnalysisDialog(GuiElement):

    def __init__(self):
        super(DataAnalysisDialog, self).__init__()

    def run(self, columns, callbacks, *, additional_checks, algorithms):
        # TODO: add list of algorithms
        self.top = tkinter.Tk(className='Data analysis options dialog')
        self.algorithms_labels = []
        self.algorithms_listboxes = []
        for algorithm in algorithms:
            self.algorithms_labels.append(Label(self.top, text=algorithm[0]))
            self.algorithms_labels[-1].pack(side=tkinter.TOP, fill=X, anchor='w')
            self.algorithms_listboxes.append(
                Spinbox(
                    self.top,
                    values=(algorithm[1])
                )
            )
            self.algorithms_listboxes[-1].pack(side=tkinter.TOP, fill=X, anchor='w')

        self.columns_label = Label(self.top, text='Apply to columns:')
        self.columns_label.pack(side=tkinter.TOP, fill=X, anchor='w')

        # add checkbuttons for all the columns in data frame
        self.column_checkboxes = []
        self.column_vars = {}
        for column in columns:
            self.column_vars[column] = BooleanVar(self.top)
            self.column_vars[column].set(False)
            self.column_checkboxes.append(Checkbutton(
                self.top,
                variable=self.column_vars[column],
                text=column,
                command=lambda: callbacks['update_process_btn'](self)
            ))
            self.column_checkboxes[-1].pack(side=tkinter.TOP, anchor='w')
        self.additional_checkboxes = []
        self.additional_vars = {}
        if len(additional_checks) > 0:
            for elem in additional_checks:
                column = elem[0]
                self.additional_vars[column] = BooleanVar(self.top)
                self.additional_vars[column].set(False)
                self.additional_checkboxes.append(Checkbutton(
                    self.top,
                    variable=self.additional_vars[column],
                    text=column,
                    command=elem[1]
                ))
                self.additional_checkboxes[-1].pack(side=tkinter.TOP, anchor='w')
        self.process_button = Button(
            self.top,
            text='process',
            command=callbacks['process_callback'],
            state='disabled'
        )
        self.process_button.pack(side=tkinter.BOTTOM, fill=X)
        self.top.update()
        self.top.mainloop()

class UserGui(GuiElement):

    def __init__(self):
        super(UserGui, self).__init__()
        self.padx = 10
        self.figure = Figure(figsize=(10, 7), dpi=100)

    def run(self, callbacks):
        """
        Runs GUI
        :return:
        """
        self.top = tkinter.Tk(className='Data analysis tools GUI')

        # menu button
        self.menu_button = Menubutton(self.top, text="File", relief=RAISED)
        self.menu_button.pack(side=tkinter.LEFT, anchor='nw', padx=3, pady=3)
        self.menu_button.menu = Menu(self.menu_button, tearoff=0)
        self.menu_button["menu"] = self.menu_button.menu
        self.menu_button.menu.add_command(label="Open",
                                command=callbacks['open_file_callback'])
        self.menu_button.menu.add_command(label="Exit",
                                command=lambda: sys.exit())

        # canvas to draw a plot onto
        self.canvas = FigureCanvasTkAgg(self.figure, self.top)
        NavigationToolbar2Tk(self.canvas, self.top)
        self.canvas.get_tk_widget().pack(side=tkinter.RIGHT, fill=X, anchor='center', padx=3, pady=3)
        self.canvas.draw()

        # process button
        self.process_menu_button = Menubutton(
            self.top, text="Processing tools", relief=RAISED, state='disabled'
        )
        self.process_menu_button.pack(anchor='nw', padx=3, pady=3)
        self.process_menu_button.menu = Menu(self.process_menu_button, tearoff=0)
        self.process_menu_button["menu"] = self.process_menu_button.menu
        self.process_menu_button.menu.add_command(
            label="Peak analysis tools",
            command=callbacks['peak_analysis_tools_callback']
        )
        self.process_menu_button.menu.add_command(
            label="Clustering tools",
            command=callbacks['clustering_tools_callback']
        )
        self.process_menu_button.menu.add_command(
            label="Classifying tools",
            command=callbacks['classifying_tools_callback']
        )
        self.process_menu_button.menu.add_command(
            label="Coupled peaks visualization",
            command=callbacks['coupled_peaks_visualization_callback']
        )

        # x axis settings
        self.x_axis_label = Label(self.top, text="X axis column")
        self.x_axis_label.pack()

        self.x_axis_title = StringVar(self.top)
        self.x_axis_title.set("")  # default value

        self.x_axis_listbox = Spinbox(
            self.top,
            values=(),
            textvariable=self.x_axis_title,
            command=lambda: callbacks['update_gui_state']()
        )
        self.x_axis_listbox.pack()

        # y axis settings
        self.y_axis_label = Label(self.top, text="Y axis column")
        self.y_axis_label.pack()

        self.y_axis_title = StringVar(self.top)
        self.y_axis_title.set("")  # default value

        self.y_axis_listbox = Spinbox(
            self.top,
            values=(),
            textvariable=self.y_axis_title,
            command=lambda: callbacks['update_gui_state']()
        )
        self.y_axis_listbox.pack()

        # color axis settings
        self.color_axis_label = Label(self.top, text="Color axis column")
        self.color_axis_label.pack()

        self.color_axis_title = StringVar(self.top)
        self.color_axis_title.set("")  # default value

        self.color_axis_listbox = Spinbox(
            self.top,
            values=(),
            textvariable=self.color_axis_title,
            command=lambda: callbacks['update_gui_state']()
        )
        self.color_axis_listbox.pack()

        self.top.update()
        self.top.mainloop()
