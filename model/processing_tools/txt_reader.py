import numpy as np
from io import StringIO

def read_from_txt(path, exclude_col=None):
    with open(path, 'r') as f:
        lines = f.readlines()
    index = 0
    for line_id, line in enumerate(lines):
        if line[0] == '%':
            index = line_id + 1
            continue
        lines[line_id] = line.replace(';', '\t')
    header = lines[index - 1]
    header = header[2:-1].split(', ')
    exclude_index = None
    for col_id, col in enumerate(header):
        header[col_id] = col.replace(' (', ',').replace(')', '')
        if exclude_col is not None:
            if exclude_col in header[col_id].lower():
                exclude_index = col_id
    data = np.loadtxt(StringIO(''.join(lines[index:])))
    if exclude_index is not None:
        data = np.delete(data, exclude_index, axis=1)
        header = header[:exclude_index] + header[exclude_index+1:]
    return header, data
