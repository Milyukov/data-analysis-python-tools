import os
import time
import pandas as pd
import numpy as np
from model.processing_tools.lvm_reader import read_from_lvm
from model.processing_tools.dat_reader import read_from_dat
from model.processing_tools.txt_reader import read_from_txt

class FromGui(object):

    def __init__(self, model, view):
        self.model = model
        self.view = view

    def read_from_gui(self, data):
        pass

def set_lims(ax, x_values, y_values):
    indecies = np.logical_not(
        np.logical_or(np.isnan(x_values), np.isnan(y_values))
    )
    if np.any(indecies):
        x_min = np.min(x_values[indecies])
        x_max = np.max(x_values[indecies])
        y_min = np.min(y_values[indecies])
        y_max = np.max(y_values[indecies])
        ax.set_xlim(x_min, x_max)
        ax.set_ylim(y_min, y_max)
    return ax

class FromUserGui(FromGui):

    def __init__(self, model, view, data_analysis_dialogs, data_analysis_plots):
        super(FromUserGui, self).__init__(model, view)
        self.current_x_column = None
        self.current_y_column = None
        self.current_color_column = None
        self.peak_analysis_dialog = data_analysis_dialogs['peak_analysis_dialog']
        self.peak_analysis_plot = data_analysis_plots['peak_analysis_plot']
        self.cluster_analysis_dialog = data_analysis_dialogs['cluster_analysis_dialog']
        self.cluster_analysis_plot = data_analysis_plots['cluster_analysis_plot']
        self.classification_dialog = data_analysis_dialogs['classification_dialog']
        self.classification_plot = data_analysis_plots['classification_plot']
        self.peak_visualization_dialog = data_analysis_dialogs['peak_visualization_dialog']
        self.peak_visualization_plot = data_analysis_plots['peak_visualization_plot']
        self.find_baseline = False
        self.train_classifier = False
        # dict of all the callbacks for each view
        self.callbacks = {
            'main_view': {
                'open_file_callback': self.open_file_callback,
                'peak_analysis_tools_callback': self.peak_analysis_tools_callback,
                'clustering_tools_callback': self.clustering_tools_callback,
                'classifying_tools_callback': self.classifying_tools_callback,
                'update_gui_state': self.update_gui_state,
                'coupled_peaks_visualization_callback': self.coupled_peaks_visualization_callback
            },
            'peak_analysis_dialog': {
                'find_baseline_check_callback': self.find_baseline_check_callback,
                'process_callback': self.peak_analysis_process_callback,
                'update_process_btn': self.update_process_btn
            },
            'peak_analysis_plot': {
             'recalculate_callback': self.recalculate_peaks_callback,
             'abs_callback': None,
             'on_exit_callback': self.on_exit_callback,
             'save_data_callback': self.save_data_callback
            },
            'cluster_analysis_dialog': {
                'process_callback': self.cluster_analysis_process_callback,
                'update_process_btn': self.update_process_btn
            },
            'cluster_analysis_plot': {
                'recalculate_callback': self.recalculate_clusters_callback,
                'on_exit_callback': self.on_exit_callback,
                'abs_callback': self.abs_callback,
                'save_data_callback': self.save_data_callback
            },
            'classification_dialog': {
                'train_classifier_callback': self.train_classifier_callback,
                'process_callback': self.classification_process_callback,
                'update_process_btn': self.update_process_btn
            },
            'classification_plot': {
                'recalculate_callback': None,
                'on_exit_callback': self.on_exit_callback,
                'abs_callback': None,
                'save_data_callback': self.save_data_callback
            },
            'peak_visualization_dialog': {
                'process_callback': self.visuzalize_peaks_callback,
                'update_process_btn': self.update_process_btn
            },
            'peak_visualization_plot': {
                'recalculate_callback': None,
                'abs_callback': None,
                'on_exit_callback': self.on_exit_callback,
                'save_data_callback': self.save_data_callback
            }
        }

    # ---- Main View callbacks ----

    def run(self):
        self.view.run(self.callbacks['main_view'])

    def save_data_callback(self):
        self.peak_analysis_plot.save_file_dialog()
        filename = self.peak_analysis_plot.saved_filename
        if filename is not None:
            if self.model.data_frame.shape[0] > 100000:
                self.model.save_large_data(filename)
            else:
                writer = pd.ExcelWriter(filename)
                self.model.data_frame.to_excel(writer, 'Processed data', index=False)
                writer.save()

    def draw_plot(self):
        """
        Draws 2D plot of chosen data columns
        :return:
        """
        self.view.figure.clear()
        ax = self.view.figure.add_subplot(111)
        if self.current_x_column == 'time':
            if self.current_y_column in self.model.data_frame:
                self.model.data_frame[self.current_y_column].plot(ax=ax)
                self.view.figure.canvas.draw()
            return
        if self.current_color_column is not None:
            if self.current_color_column == 'type':
                self.model.data_frame = self.model.data_frame.replace(
                    {
                        value: num
                        for num, value in
                        enumerate(np.unique(self.model.data_frame[self.current_color_column].values))
                    }
                )
                self.model.data_frame = self.model.data_frame.apply(pd.to_numeric, args=('coerce',))
            self.model.data_frame.plot.scatter(
                x=self.current_x_column,
                y=self.current_y_column,
                c=self.current_color_column,
                colormap='viridis', ax=ax
            )
        else:
            self.model.data_frame.plot.scatter(
                x=self.current_x_column, y=self.current_y_column, ax=ax
            )
        ax.ticklabel_format(style='sci', axis='x', scilimits=(-3, 9))
        ax.ticklabel_format(style='sci', axis='y', scilimits=(-3, 9))
        x_values = self.model.data_frame[self.current_x_column].values
        y_values = self.model.data_frame[self.current_y_column].values
        set_lims(ax, x_values, y_values)
        self.view.figure.canvas.draw()

    def open_file_callback(self):
        """
        Runs open file dialog to open read data from file
        :return:
        """
        filenames = self.view.open_file_dialog()
        #filenames = sorted(filenames, key=lambda x: int(list(filter(str.isdigit, x))[0]))
        if len(filenames) > 0:
            self.model.clear()
        for filename in filenames:
            if filename.lower().endswith('.xlsx'):
                if self.model.data_frame.size == 0:
                    self.model.set_data_frame(pd.read_excel(filename))
                else:
                    self.model.concatenate(pd.read_excel(filename))
            elif filename.lower().endswith('.csv'):
                if self.model.data_frame.size == 0:
                    self.model.set_data_frame(pd.read_csv(filename))
                else:
                    self.model.concatenate(pd.read_csv(filename))
            elif filename.lower().endswith('.dat'):
                directory, file = os.path.split(filename)
                header, data = read_from_dat(directory, [file])
                concatenated_data = None
                for key in data:
                    if concatenated_data is None:
                        concatenated_data = data[key]
                    else:
                        concatenated_data = np.concatenate((concatenated_data, data[key]))
                if self.model.data_frame.size == 0:
                    self.model.set_data_frame(pd.DataFrame(columns=header, data=concatenated_data))
                else:
                    self.model.concatenate(pd.DataFrame(columns=header, data=concatenated_data))
            elif filename.lower().endswith('.npy'):
                self.model.load_large_data(filename)
            elif filename.lower().endswith('.lvm'):
                header, data = read_from_lvm([filename])
                concatenated_data = None
                for key in data:
                    if concatenated_data is None:
                        concatenated_data = data[key]
                    else:
                        concatenated_data = np.concatenate((concatenated_data, data[key]))
                if self.model.data_frame.size == 0:
                    self.model.set_data_frame(pd.DataFrame(columns=header, data=concatenated_data))
                else:
                    self.model.concatenate(pd.DataFrame(columns=header, data=concatenated_data))
            elif filename.lower().endswith('.txt'):
                header, data = read_from_txt(filename, exclude_col='time')
                if self.model.data_frame.size == 0:
                    self.model.set_data_frame(pd.DataFrame(columns=header, data=data))
                else:
                    self.model.concatenate(pd.DataFrame(columns=header, data=data))
            else:
                header, data = read_from_lvm([filename], delimiter=' ', symbols_to_delete=['[', ']', '#'])
                concatenated_data = None
                for key in data:
                    if concatenated_data is None:
                        concatenated_data = data[key]
                    else:
                        concatenated_data = np.concatenate((concatenated_data, data[key]))
                if self.model.data_frame.size == 0:
                    self.model.set_data_frame(pd.DataFrame(columns=header, data=concatenated_data))
                else:
                    self.model.concatenate(pd.DataFrame(columns=header, data=concatenated_data))

        # fill axis listbox
        # self.model.save_large_data(
        #     '/home/gleb/_work/projects/Dresden/data-analysis-python-tools/data/Impedance data for Gleb/data.npy')
        # self.model.load_large_data(
        #     '/home/gleb/_work/projects/Dresden/data-analysis-python-tools/data/Impedance data for Gleb/data.npy')
        values = tuple(map(tuple, self.model.data_frame.columns))
        self.view.x_axis_listbox.config(values=values + ('time',))
        self.view.y_axis_listbox.config(values=values)
        self.view.color_axis_listbox.config(values=values)

        self.update_gui_state()

    def update_gui_state(self):
        # ---- define axes  ----
        columns = self.model.data_frame.columns.values
        self.current_x_column = self.view.x_axis_listbox.get()
        self.current_x_column = self.current_x_column.replace(' ', '')
        if self.current_x_column is None:
            self.current_x_column = columns[0]
        self.current_y_column = self.view.y_axis_listbox.get()
        self.current_y_column = self.current_y_column.replace(' ', '')
        if self.current_y_column is None:
            self.current_y_column = columns[1]
        self.current_color_column = self.view.color_axis_listbox.get()
        self.current_color_column = self.current_color_column.replace(' ', '')
        if self.current_color_column is None and columns.shape[0] > 2:
            self.current_color_column = columns[-1]

        # ---- change flags ----
        if not self.model.data_frame.empty:
            self.view.process_menu_button.config(state='normal')
        else:
            self.view.process_menu_button.config(state='disabled')

        # ----   draw plot  ----
        self.draw_plot()

    def clustering_tools_callback(self):
        self.cluster_analysis_dialog.run(
            self.model.data_frame.columns,
            self.callbacks['cluster_analysis_dialog'],
            additional_checks=[],
            algorithms=[
                ('Clustering algorithms:', ['Density analysis', 'K-Means'])
            ]
        )

    def classifying_tools_callback(self):
        data_frame_column_names = tuple(map(tuple, self.model.data_frame.columns))
        names = []
        self.train_classifier = False
        for name in data_frame_column_names:
            names.append(''.join(name))
        self.classification_dialog.run(
            self.model.data_frame.columns,
            self.callbacks['classification_dialog'],
            additional_checks=[
                (
                    "Train new classifier",
                    self.callbacks['classification_dialog']['train_classifier_callback']
                )
            ],
            algorithms=[
                ('Classification algorithms:', ['Decision Tree']),
                ('Labels for training:', names),
            ]
        )

    def coupled_peaks_visualization_callback(self):
        self.peak_visualization_dialog.run(
            self.model.data_frame.columns,
            self.callbacks['peak_visualization_dialog'],
            additional_checks=[],
            algorithms=[]
        )

    def peak_analysis_tools_callback(self):
        self.peak_analysis_dialog.run(
            self.model.data_frame.columns,
            self.callbacks['peak_analysis_dialog'],
            additional_checks=[
                (
                    "Find and substract baseline",
                    self.callbacks['peak_analysis_dialog']['find_baseline_check_callback']
                )
            ],
            algorithms=[
                ('Baseline detection algorithms:', ['UnivariateSpline', 'Fourier', 'Polynomial', 'SavitzkyGolay']),
                ('Peak detection algorithms:', ['IQR', 'Derivative'])
            ]
        )

    # ---- End of Main View callbacks ----

    # ---- Peak Analysis callbacks ----

    def find_baseline_check_callback(self):
        self.find_baseline = not self.find_baseline

    def update_process_btn(self, gui_elem):
        flag = False
        checkbuttons = gui_elem.column_vars
        for c in checkbuttons:
            flag |= checkbuttons[c].get()
        if flag:
            gui_elem.process_button.config(state='normal')
        else:
            gui_elem.process_button.config(state='disabled')

    def visuzalize_peaks_callback(self):
        checkbuttons = self.peak_visualization_dialog.column_vars
        data_to_plot = []
        self.classifier = None
        self.columns_to_visualize = []
        for c in checkbuttons:
            if checkbuttons[c].get():
                self.columns_to_visualize.append(c)
        self.columns_to_visualize = sorted(self.columns_to_visualize)
        for c in self.columns_to_visualize:
            data_to_plot.append(
                {
                    'signal': self.model.data_frame.as_matrix(columns=[c])[:, 0],
                    'peaks': self.model.data_frame.as_matrix(columns=['{}_peaks'.format(c)])[:, 0],
                    'column_name': c
                }
            )
        self.peak_visualization_dialog.top.destroy()
        self.peak_visualization_plot.run(
            '',
            self.callbacks['peak_visualization_plot'],
            self.model.data_frame.columns
        )
        self.visualize_peaks(data_to_plot)

    def visualize_peaks(self, columns):
        self.peak_visualization_plot.figure.clear()
        ax1 = self.peak_visualization_plot.figure.add_subplot(111)
        ax2 = ax1.twinx()
        axes = [ax1, ax2]
        colors = ['tab:green', 'tab:blue']
        peaks_indices = np.logical_not(np.isnan(columns[0]['peaks']))
        for col in columns[1:]:
            peaks_indices = np.logical_and(np.logical_not(np.isnan(col['peaks'])), peaks_indices)
        for id, col in enumerate(columns):
            x = [i for i in range(len(col['signal']))]
            axes[id].plot(x, col['signal'] - np.mean(col['signal']), label='{}'.format(col['column_name']), color=colors[id])
            axes[id].scatter(x=np.nonzero(peaks_indices)[0], y=col['signal'][peaks_indices] - np.mean(col['signal']), c='r')
            axes[id].set_xlabel('Sample number')
            axes[id].set_ylabel('{}'.format(col['column_name']))

        ax1.ticklabel_format(style='sci', axis='x', scilimits=(-3, 9))
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(-3, 9))
        ax2.ticklabel_format(style='sci', axis='x', scilimits=(-3, 9))
        ax2.ticklabel_format(style='sci', axis='y', scilimits=(-3, 9))
        ax1.legend(loc='best')
        ax2.legend(loc='best')
        self.peak_visualization_plot.figure.canvas.draw()

    def draw_peak_data(self, columns):
        number_of_columns = len(columns)
        if number_of_columns == 1:
            number_of_rows = 1
            number_of_cols = 1
        elif number_of_columns < 3:
            number_of_rows = 2
            number_of_cols = 1
        elif number_of_columns < 5:
            number_of_rows = 2
            number_of_cols = 2
        elif number_of_columns < 7:
            number_of_rows = 2
            number_of_cols = 3
        elif number_of_columns < 10:
            number_of_rows = 3
            number_of_cols = 3
        else:
            raise NotImplementedError('Too many columns to plot')
        self.peak_analysis_plot.figure.clear()
        # TODO: change
        coupled_peaks_indices = None
        for col in columns:
            if coupled_peaks_indices is None:
                coupled_peaks_indices = np.logical_not(np.isnan(col['peaks']))
            else:
                coupled_peaks_indices = np.logical_and(coupled_peaks_indices, coupled_peaks_indices)
        for id, col in enumerate(columns):
            if col['baseline'] is not None:
                xs = [i for i in range(len(col['signal']))]
                ax = self.peak_analysis_plot.figure.add_subplot(
                    number_of_rows * 2, number_of_cols, 2 * id + 1
                )
                ax.plot(xs, col['signal'])
                ax.plot(xs, col['baseline'])
                x = coupled_peaks_indices
                ax.scatter(x=np.nonzero(x)[0], y=col['signal'][x], c='r')
                ax.set_ylabel(col['column_name'])
                ax.set_xlabel('Sample number')
                ax.ticklabel_format(style='sci', axis='x', scilimits=(-3, 9))
                ax.ticklabel_format(style='sci', axis='y', scilimits=(-3, 9))
                ax = self.peak_analysis_plot.figure.add_subplot(
                    number_of_rows * 2, number_of_cols, 2 * id + 1 + number_of_cols
                )
                ax.plot(xs, col['signal'] - col['baseline'])
                ax.scatter(x=np.nonzero(x)[0], y=col['signal'][x] - col['baseline'][x], c='r')
            else:
                xs = [i for i in range(len(col['signal']))]
                ax = self.peak_analysis_plot.figure.add_subplot(
                    number_of_rows, number_of_cols, id + 1
                )
                ax.plot(xs, col['signal'])
                x = coupled_peaks_indices
                ax.scatter(x=np.nonzero(x)[0], y=col['signal'][x], c='r')
            for iqr_label in col['iqr_area']:
                iqr_y = col['iqr_area'][iqr_label]
                ax.plot([0, len(xs)], [iqr_y, iqr_y], '--o', label=iqr_label)
            ax.set_ylabel('{} with subtracted baseline'.format(col['column_name']))
            ax.set_xlabel('Sample number')
        ax.ticklabel_format(style='sci', axis='x', scilimits=(-3, 9))
        ax.ticklabel_format(style='sci', axis='y', scilimits=(-3, 9))
        ax.legend()
        self.peak_analysis_plot.figure.canvas.draw()

    def recalculate_peaks_callback(self):
        data_to_plot = []
        for c in self.columns_to_find_peaks:
            if self.find_baseline:
                baseline_params_changed = False
                for param, label in zip(self.peak_analysis_plot.algorithm_spinboxes[0],
                                        self.peak_analysis_plot.algorithm_labels[0]):
                    if self.model.approximator[self.approximator].parameters[label['text']].value != int(param.get()):
                        self.model.approximator[self.approximator].parameters[label['text']].value = int(param.get())
                        baseline_params_changed = True
                if baseline_params_changed:
                    self.model.find_baseline(c, self.approximator)
            for param, label in zip(self.peak_analysis_plot.algorithm_spinboxes[1],
                                    self.peak_analysis_plot.algorithm_labels[1]):
                self.model.peak_finder[self.peak_finder].parameters[label['text']].value = float(param.get())
            self.model.find_peaks(c, self.peak_finder)
            data_to_plot.append(
                {
                    'signal': self.model.data_frame.as_matrix(columns=[c])[:, 0],
                    'baseline': self.model.data_frame.as_matrix(columns=['{}_baseline'.format(c)]
                                )[:, 0] if self.find_baseline else None,
                    'peaks': self.model.data_frame.as_matrix(columns=['{}_peaks'.format(c)])[:, 0],
                    'iqr_area': self.model.peak_finder[self.peak_finder].iqr_area,
                    'column_name': c
                }
            )
        self.draw_peak_data(data_to_plot)

    def on_exit_callback(self, gui_elem):
        self.find_baseline = False
        # TODO: backuping of big files is a very slow process
        # if not os.path.exists('backup'):
        #     os.mkdir('backup')
        # df = self.model.data_frame.fillna('--')
        # writer = pd.ExcelWriter('backup/backup_{}.xlsx'.format(time.time()))
        # df.to_excel(writer, 'Processed data')
        # writer.save()

        # fill axis listbox
        values = tuple(map(tuple, self.model.data_frame.columns))
        self.view.x_axis_listbox.config(values=values + ('time',))
        self.view.y_axis_listbox.config(values=values)
        self.view.color_axis_listbox.config(values=values)

        gui_elem.destroy()

    def peak_analysis_process_callback(self):
        checkbuttons = self.peak_analysis_dialog.column_vars
        data_to_plot = []
        self.approximator = None
        self.peak_finder = None
        self.columns_to_find_peaks = []
        for c in checkbuttons:
            if checkbuttons[c].get():
                self.columns_to_find_peaks.append(c)
                if self.find_baseline:
                    self.approximator = self.peak_analysis_dialog.algorithms_listboxes[0].get()
                    self.model.find_baseline(c, self.approximator)
                self.peak_finder = self.peak_analysis_dialog.algorithms_listboxes[1].get()
                self.model.find_peaks(c, self.peak_finder)
                data_to_plot.append(
                    {
                        'signal': self.model.data_frame.as_matrix(columns=[c])[:, 0],
                        'baseline': self.model.data_frame.as_matrix(
                            columns=['{}_baseline'.format(c)]
                        )[:, 0] if self.find_baseline else None,
                        'peaks': self.model.data_frame.as_matrix(columns=['{}_peaks'.format(c)])[:, 0],
                        'iqr_area': self.model.peak_finder[self.peak_finder].iqr_area,
                        'column_name': c
                    }
                )
        self.peak_analysis_dialog.top.destroy()
        if self.find_baseline:
            algorithms = [self.model.approximator[self.approximator],
             self.model.peak_finder[self.peak_finder]]
        else:
            algorithms = [self.model.peak_finder[self.peak_finder]]
        self.peak_analysis_plot.run(
            algorithms,
            self.callbacks['peak_analysis_plot'],
            self.model.data_frame.columns
        )
        self.draw_peak_data(data_to_plot)

    # ---- End of Peak Analysis callbacks ----

    # ---- Data Clustering callbacks ----

    def draw_cluster_data(self, columns, show_absolute_values):
        number_of_columns = len(columns)
        if number_of_columns == 1:
            number_of_rows = 1
            number_of_cols = 1
        elif number_of_columns < 3:
            number_of_rows = 2
            number_of_cols = 1
        elif number_of_columns < 5:
            number_of_rows = 2
            number_of_cols = 2
        elif number_of_columns < 7:
            number_of_rows = 2
            number_of_cols = 3
        elif number_of_columns < 10:
            number_of_rows = 3
            number_of_cols = 3
        else:
            raise NotImplementedError('Too many columns to plot')
        self.cluster_analysis_plot.figure.clear()
        for id, column in enumerate(columns):
            ax = self.cluster_analysis_plot.figure.add_subplot(number_of_rows, number_of_cols, id + 1)
            x_values = column['peaks_values'][:, 1]
            y_values = column['peaks_values'][:, 0]
            if show_absolute_values:
                x_values = np.abs(x_values)
                y_values = np.abs(y_values)
            indicies = np.logical_and(
                np.logical_not(np.isnan(x_values)),
                np.logical_not(np.isnan(y_values))
            )
            for cluster_id in np.unique(column['cluster_ids'][:]):
                cluster_indicies = np.logical_and(column['cluster_ids'] == cluster_id, indicies)
                ratio = len(column['cluster_ids'][cluster_indicies]) / len(column['cluster_ids'][indicies])
                ax.scatter(
                    x=x_values[cluster_indicies],
                    y=y_values[cluster_indicies],
                    label="%5.2f" % (100 * ratio,) + '%',
                    cmap='viridis'
                )
            ax.legend()
            set_lims(ax, x_values[indicies], y_values[indicies])
            if show_absolute_values:
                ax.set_ylabel('{} absolute value'.format(column['column_name'][0]))
                ax.set_xlabel('{}, absolute value'.format(column['column_name'][1]))
            else:
                ax.set_ylabel(column['column_name'][0])
                ax.set_xlabel(column['column_name'][1])
        ax.ticklabel_format(style='sci', axis='x', scilimits=(-3, 9))
        ax.ticklabel_format(style='sci', axis='y', scilimits=(-3, 9))
        self.cluster_analysis_plot.figure.canvas.draw()

    def abs_callback(self):
        data_to_plot = []
        self.show_absolute_values = not self.show_absolute_values
        # This isn't a bug but a feature
        if self.show_absolute_values:
            indecies = self.model.data_frame.as_matrix(
                columns=['{}_{}_clusterized'.format(*self.columns_to_visualize)])[:, 0]
        else:
            indecies = self.model.data_frame.as_matrix(
                columns=['{}_{}_abs_clusterized'.format(*self.columns_to_visualize)])[:, 0]
        data_to_plot.append(
            {
                'peaks_values': self.model.data_frame.as_matrix(columns=self.columns_to_visualize),
                'cluster_ids': indecies,
                'column_name': self.columns_to_visualize
            }
        )
        self.draw_cluster_data(data_to_plot, show_absolute_values=self.show_absolute_values)

    def recalculate_clusters_callback(self):
        data_to_plot = []
        for param, label in zip(self.cluster_analysis_plot.algorithm_spinboxes[0],
                                self.cluster_analysis_plot.algorithm_labels[0]):
            self.model.clusterizator[self.clusterizator].parameters[label['text']].value = float(param.get())
        self.model.clusterize(self.columns_to_visualize, self.clusterizator, self.show_absolute_values)
        if self.show_absolute_values:
            indecies = self.model.data_frame.as_matrix(
                columns=['{}_{}_abs_clusterized'.format(*self.columns_to_visualize)])[:, 0]
        else:
            indecies = self.model.data_frame.as_matrix(
                columns=['{}_{}_clusterized'.format(*self.columns_to_visualize)])[:, 0]
        data_to_plot.append(
            {
                'peaks_values': self.model.data_frame.as_matrix(columns=self.columns_to_visualize),
                'cluster_ids': indecies,
                'column_name': self.columns_to_visualize
            }
        )
        self.draw_cluster_data(data_to_plot, show_absolute_values=self.show_absolute_values)

    def cluster_analysis_process_callback(self):
        checkbuttons = self.cluster_analysis_dialog.column_vars
        data_to_plot = []
        self.clusterizator = None
        self.show_absolute_values = False
        self.columns_to_visualize = []
        for c in checkbuttons:
            if checkbuttons[c].get():
                self.columns_to_visualize.append(c)
        self.columns_to_visualize = sorted(self.columns_to_visualize)
        self.clusterizator = self.cluster_analysis_dialog.algorithms_listboxes[0].get()
        self.model.clusterize(self.columns_to_visualize, self.clusterizator)
        indecies = self.model.data_frame.as_matrix(columns=['{}_{}_clusterized'.format(*self.columns_to_visualize)])[:, 0]
        data_to_plot.append(
            {
                'peaks_values': self.model.data_frame.as_matrix(columns=self.columns_to_visualize),
                'cluster_ids': indecies,
                'column_name': self.columns_to_visualize
            }
        )
        self.cluster_analysis_dialog.top.destroy()

        self.cluster_analysis_plot.run(
            [self.model.clusterizator[self.clusterizator]],
            self.callbacks['cluster_analysis_plot'],
            self.model.data_frame.columns
        )
        self.draw_cluster_data(data_to_plot, show_absolute_values=False)

    # ---- End of Data Clustering callbacks ----

    # ---- Data Classification callbacks ----

    def train_classifier_callback(self):
        self.train_classifier = not self.train_classifier

    def classification_process_callback(self):
        checkbuttons = self.classification_dialog.column_vars
        data_to_plot = []
        self.classifier = None
        self.columns_to_visualize = []
        for c in checkbuttons:
            if checkbuttons[c].get():
                self.columns_to_visualize.append(c)
        self.columns_to_visualize = sorted(self.columns_to_visualize)
        self.classifier = self.classification_dialog.algorithms_listboxes[0].get()
        if self.train_classifier:
            self.labels = self.classification_dialog.algorithms_listboxes[1].get()
            self.model.train_classifier(
                self.columns_to_visualize, self.labels,
                self.classifier
            )
            self.classification_dialog.save_classifier_dialog()
            filename = self.classification_dialog.saved_classifier_filename
            self.model.classifier[self.classifier].save(filename)

        else:
            filename = self.classification_dialog.open_classifier_dialog()
            self.model.classifier[self.classifier].load(filename)
            self.model.classify(self.columns_to_visualize, self.classifier)
            indecies = self.model.data_frame.as_matrix(columns=['{}_{}_classified'.format(*self.columns_to_visualize)])[:, 0]
            data_to_plot.append(
                {
                    'peaks_values': self.model.data_frame.as_matrix(columns=self.columns_to_visualize),
                    'class_ids': indecies.astype(np.int),
                    'label_names': self.model.classifier[self.classifier].label_names,
                    'column_name': self.columns_to_visualize
                }
            )
            self.classification_dialog.top.destroy()

            self.classification_plot.run(
                [self.model.classifier[self.classifier]],
                self.callbacks['classification_plot'],
                self.model.data_frame.columns
            )
            self.draw_class_data(data_to_plot)

    def draw_class_data(self, columns):
        number_of_columns = len(columns)
        if number_of_columns == 1:
            number_of_rows = 1
            number_of_cols = 1
        elif number_of_columns < 3:
            number_of_rows = 2
            number_of_cols = 1
        elif number_of_columns < 5:
            number_of_rows = 2
            number_of_cols = 2
        elif number_of_columns < 7:
            number_of_rows = 2
            number_of_cols = 3
        elif number_of_columns < 10:
            number_of_rows = 3
            number_of_cols = 3
        else:
            raise NotImplementedError('Too many columns to plot')
        self.classification_plot.figure.clear()
        for id, column in enumerate(columns):
            ax = self.classification_plot.figure.add_subplot(
                number_of_rows, number_of_cols, id + 1
            )
            for class_id in np.unique(column['class_ids'][:]):
                class_indicies = column['class_ids'] == class_id
                ratio = len(column['class_ids'][class_indicies]) / len(column['class_ids'])
                percent_str = "%5.2f" % (100 * ratio,)
                ax.scatter(
                    x=column['peaks_values'][:, 0][class_indicies],
                    y=column['peaks_values'][:, 1][class_indicies],
                    label='{0}, {1}%'.format(column['label_names'][class_id], percent_str)
                )
            ax.set_ylabel(column['column_name'][1])
            ax.set_xlabel(column['column_name'][0])
            ax.legend()
            peak_values = column['peaks_values']
            x_values = peak_values[np.logical_not(np.isnan(peak_values[:, 0])), 0]
            x_min = np.min(x_values)
            x_max = np.max(x_values)
            y_values = peak_values[np.logical_not(np.isnan(peak_values[:, 1])), 1]
            y_min = np.min(y_values)
            y_max = np.max(y_values)
            ax.set_xlim(x_min, x_max)
            ax.set_ylim(y_min, y_max)
        ax.ticklabel_format(style='sci', axis='x', scilimits=(-3, 9))
        ax.ticklabel_format(style='sci', axis='y', scilimits=(-3, 9))
        self.classification_plot.figure.canvas.draw()

    # ---- End of Data Classification callbacks ----
