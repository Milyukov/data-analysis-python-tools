from model.processing_tools.approximation import *
from model.processing_tools.peak_analysis import *
from model.processing_tools.clusterization import *
from model.processing_tools.classification import *
import pandas as pd
import pickle

class SignalModel(object):

    def __init__(self):
        self.data_frame = pd.DataFrame()
        self.approximator ={
            'UnivariateSpline': UnivariateSplineApproximator(),
            'Fourier': Fourier(),
            'Polynomial': Polynomial(),
            'SavitzkyGolay': SavitzkyGolay()
        }
        self.peak_finder = {
            'IQR': OutliersDetector(),
            'Derivative': DerivativeDetector()
        }
        self.clusterizator = {
            'Density analysis': DensityAnalysis(),
            'K-Means': KMeansAnalysis()
        }
        self.classifier = {
            'Decision Tree': DecisionTree()
        }
        self.label_names = {}

    def clear(self):
        self.data_frame = pd.DataFrame()

    def concatenate(self, data_frame, axis=1):
        self.data_frame = pd.concat([self.data_frame, data_frame], axis=axis, ignore_index=False)

    def set_data_frame(self, data_frame):
        self.data_frame = process_data(data_frame)

    def save_large_data(self, filename):
        np.save(filename, self.data_frame)
        meta = self.data_frame.index, self.data_frame.columns
        s = pickle.dumps(meta)
        s = "".join(map(chr, s))
        with open(filename, 'a') as f:
            f.seek(0, 2)
            f.write(s)

    def load_large_data(self, filename):
        values = np.load(filename, mmap_mode='r')
        with open(filename) as f:
            f.seek(values.dtype.alignment * values.size, 1)
            meta = pickle.loads(f.readline())
            self.data_frame = pd.DataFrame(values, index=meta[0], columns=meta[1])

    def find_baseline(self, column_name, algorithm_name):
        if column_name in self.data_frame:
            baseline = self.approximator[algorithm_name].find_baseline(
                self.data_frame.as_matrix(columns=[column_name])[:, 0]
            )
            self.data_frame['{}_baseline'.format(column_name)] = pd.Series(baseline)
            self.data_frame['{}_subtracted'.format(column_name)] = (self.data_frame.as_matrix(columns=[column_name])[:, 0] -
                      self.data_frame.as_matrix(columns=['{}_baseline'.format(column_name)])[:, 0])

    def find_peaks(self, column_name, algorithm_name, ignore_zeros=False):
        if column_name in self.data_frame:
            signal = self.data_frame.as_matrix(columns=[column_name])[:, 0]
            if '{}_baseline'.format(column_name) in self.data_frame:

                signal = self.data_frame.as_matrix(columns=['{}_subtracted'.format(column_name)])[:, 0]

                peaks = self.peak_finder[algorithm_name].detect(signal, ignore_zeros)[0]
            else:
                peaks = self.peak_finder[algorithm_name].detect(signal, ignore_zeros)[0]
            peaks_to_save = np.zeros_like(self.data_frame[column_name].values)
            peaks_to_save[peaks] = 1

            peaks_values = signal[peaks_to_save > 0]

            peak_values_to_save = np.ones_like(peaks_to_save) * np.nan
            peak_values_to_save[peaks_to_save > 0] = peaks_values

            col = '{}_peaks'.format(column_name)
            self.data_frame[col] = pd.Series(
                peak_values_to_save
            )

            normalized_data, mean_val, std_val = normalize_data(peaks_values)
            normalized_peaks_to_save = np.ones_like(peaks_to_save) * np.nan
            normalized_peaks_to_save[peaks_to_save > 0] = normalized_data
            self.data_frame['{}_normalized'.format(col)] = pd.Series(normalized_peaks_to_save)
            self.data_frame['{}_mean_val'.format(col)] = pd.Series(mean_val)
            self.data_frame['{}_std_val'.format(col)] = pd.Series(std_val)

    def clusterize(self, column_names, algorithm_name, show_absolute_values=False):
        flag = True
        column_names = sorted(column_names)
        for column in column_names:
            if column not in self.data_frame:
                flag = False
        if flag:
            clusters = self.clusterizator[algorithm_name].clusterize(
                    data=self.data_frame.as_matrix(columns=column_names),
                    show_absolute_values=show_absolute_values
                )
            if show_absolute_values:
                new_column_name = '{}_{}_abs_clusterized'.format(*column_names)
            else:
                new_column_name = '{}_{}_clusterized'.format(*column_names)
            self.data_frame[new_column_name] = pd.Series(
                clusters,
                self.data_frame.index
            )

    def train_classifier(self, column_names, label_name, algorithm_name):
        flag = True
        column_names = sorted(column_names)
        for column in column_names:
            if column not in self.data_frame:
                flag = False
        if flag:
            data = self.data_frame.as_matrix(columns=column_names)
            features = data[:, :2]
            labels = self.data_frame.as_matrix(columns=[label_name])[:, 0]
            label_names = np.unique(labels)
            label_names_dict = {}
            index = 1
            for name in label_names:
                labels[labels == name] = index
                label_names_dict[index] = name
                index += 1
            train_labels = labels.astype(np.uint8)

            self.classifier[algorithm_name].train(
                features=features, labels=train_labels, label_names=label_names_dict
            )

    def classify(self, column_names, algorithm_name):
        flag = True
        column_names = sorted(column_names)
        for column in column_names:
            if column not in self.data_frame:
                flag = False
        if flag:
            classes = self.classifier[algorithm_name].classify(
                data=self.data_frame.as_matrix(columns=column_names)
            )
            self.data_frame['{}_{}_classified'.format(*column_names)] = pd.Series(
                classes,
                self.data_frame.index
            )
