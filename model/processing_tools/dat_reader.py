import numpy as np
import re
import os

from io import StringIO

def read_from_dat(path, filenames=[], description='', numbered=False):
    # regular expressions for files parsing
    description_file_pattern = '\d+ms.*\d+V'
    dat_number = '\d+'
    # parse *.txt file with content description
    if False:
        if description == '':
            description = [filename for filename in os.listdir(path) if filename.lower().endswith('.txt')]
            for file in description:
                with open(os.path.join(path, file), 'r') as candidate:
                    text = candidate.read()
                    if bool(re.search(description_file_pattern, text)):
                        description = file
    # initialize dictionary of arrays to store data in
    data = {}
    # if not specified read all the *.dat files from path
    if filenames == []:
        filenames = [filename for filename in os.listdir(path) if filename.lower().endswith('.dat')]
    for filenum, filename in enumerate(filenames):
        f = open(os.path.join(path, filename), mode='rb')
        lines = f.readlines()
        info = lines[0]
        header = lines[1]
        header = header.decode("latin-1").split('\t')
        for num, title in enumerate(header):
            header[num] = title.strip('\n')
        text = b'\n'.join(lines[2:])
        text = text.decode("utf-8").replace('\t', ' ')
        # get key from filename
        if numbered:
            key = re.search(dat_number, filename)
            data[int(filename[key.start():key.end()])] = np.loadtxt(StringIO(text))
        else:
            data[filenum] = np.loadtxt(StringIO(text))
    return header, data
